package com.api.v1.training.user.serviceImpl;

import static com.api.v1.training.user.util.UserConstantsUtil.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.*;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.Role;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.UsercourseSubsciption;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;
import com.api.v1.training.user.exception.ResourceNotFoundException;
import com.api.v1.training.user.exception.TrainingSiteUserApplicationException;
import com.api.v1.training.user.repository.UserProfileRepository;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.request.CourseRequest;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.response.ErrorData;
import com.api.v1.training.user.service.CustomerService;
import com.api.v1.training.user.repository.*;
import com.api.v1.training.user.domain.*;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserProfileRepository userProfileRepository;
        @Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private CourseversionRepository courseversionRepository;


	@Transactional
	@Override
	public User register(CustomerRequest request) {

		User user = request.getUser();
		user.setStatus(String.valueOf(STATUS_WAITING));
		user.setRole(new Role(1));
		user.setResetpasswordstatus(String.valueOf(STATUS_WAITING));
		userRepository.save(user);
		LOGGER.info(SAVED_USER, user.getUsername());
		Userprofile userProfile = request.getUserprofile();
		userProfile.setProfileupdatestatus(String.valueOf(STATUS_WAITING));

		Integer profileId = findNextProfileId(user.getUserid());
		LOGGER.info(NEXT_PROFILE_ID, profileId);

		userProfile.setId(new UserprofileId(user.getUserid(), profileId));
		userProfileRepository.save(userProfile);
		LOGGER.info(SAVED_USER_PROFILE, user.getUsername());
		user.setProfileversion(profileId);
		userRepository.save(user);
		LOGGER.info(UPDATED_USER_PROFILE_ID, user.getUsername());

		return user;
	}

	private Integer findNextProfileId(Integer userId) {
		Optional<User> user = userRepository.findById(userId);
		if (!user.isPresent()) {
			LOGGER.error(USER_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(USER_NOT_FOUND));
		}
		User userdata=user.get();

		List<Userprofile> userProfileList = userProfileRepository.findByuserOrderByIdAsc(userdata);
		if (!userProfileList.isEmpty()) {

			return userProfileList.get(userProfileList.size() - 1).getId().getProfileversion() + 1;

		} else {
			return 1;
		}
	}

	public List<User> getAllUsers() {
		List<User> customer = userRepository.findAll();
		return customer;
	}

	@Override

	public Optional<User> findById(int userId) {
		Optional<User> user = userRepository.findById(userId);
		return user;
	}

	@Override
	public Userprofile addUserProfile(String username, Userprofile newprofile) {
		Optional<User> user = userRepository.findByusername(username);
		if (!user.isPresent()) {
			LOGGER.error(USER_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(USER_NOT_FOUND));
		}
		User userdata = user.get();
		Optional<Userprofile> profile = userProfileRepository.findByuserAndProfileupdatestatus(userdata,
				String.valueOf(STATUS_APPROVED));

		if (!profile.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(USER_NOT_FOUND));
		}
		Userprofile oldProfile = profile.get();

		newprofile.setCity(
				StringUtils.isEmpty(newprofile.getCity()) == true ? oldProfile.getCity() : newprofile.getCity());
		newprofile.setState(
				StringUtils.isEmpty(newprofile.getState()) == true ? oldProfile.getState() : newprofile.getState());
		newprofile.setCountry(StringUtils.isEmpty(newprofile.getCountry()) == true ? oldProfile.getCountry()
				: newprofile.getCountry());
		newprofile.setFirstname(StringUtils.isEmpty(newprofile.getFirstname()) == true ? oldProfile.getFirstname()
				: newprofile.getFirstname());
		newprofile.setLastname(StringUtils.isEmpty(newprofile.getLastname()) == true ? oldProfile.getLastname()
				: newprofile.getLastname());
		newprofile.setEmail(
				StringUtils.isEmpty(newprofile.getEmail()) == true ? oldProfile.getEmail() : newprofile.getEmail());
		newprofile.setMobile(
				StringUtils.isEmpty(newprofile.getMobile()) == true ? oldProfile.getMobile() : newprofile.getMobile());

		newprofile.setProfileupdatestatus(String.valueOf(STATUS_WAITING));
		newprofile.setId(new UserprofileId(userdata.getUserid(), findNextProfileId(userdata.getUserid())));
		userProfileRepository.save(newprofile);
		LOGGER.info(SAVED_UPDATED_USER_PROFILE, userdata.getUsername());
		return newprofile;
	}

	@Override
	public Set<UsercourseSubsciption> retrieveCourseSubcriptionByUser(String username) {
	
		Optional<User> user = userRepository.findByusername(username);
		if (!user.isPresent()) {
			LOGGER.error(USER_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(USER_NOT_FOUND));
		}
		User userdata = user.get();
		Set<UsercourseSubsciption> userCourseSubscription = userdata.getUsercourseSubsciptions();
		
		if(userCourseSubscription.isEmpty())
		{
			throw new ResourceNotFoundException(new ErrorData("Courses not subscribed"));
		}
		return userCourseSubscription;
	}

	@Override
	public Courseversion retrieveSubscribedCourseContent(int userId,int courseId) {
		
		
		Optional<User> user = userRepository.findById(userId);
		if (!user.isPresent()) {
			LOGGER.error(USER_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(USER_NOT_FOUND));
		}
			User userdata = user.get();
			Optional<Course> course=courseRepository.findById(courseId);
			if (!course.isPresent())
			{
				LOGGER.error("Course Not Found");
				throw new ResourceNotFoundException(new ErrorData("Course Not Found"));
			}
			Course coursedata=course.get();
			
			Set<UsercourseSubsciption> courseSubscriptionList = userdata.getUsercourseSubsciptions();
			Set<Courseversion> courseversionList=coursedata.getCourseversions();
			if(courseSubscriptionList==null)
			{
			     throw new ResourceNotFoundException(new ErrorData("list of course subscription Not Found"));
			
			}
			if(courseversionList==null)
			{
			     throw new ResourceNotFoundException(new ErrorData("courseverionList Not Found"));
			
			}
			 UsercourseSubsciption usercourseSubcription=courseSubscriptionList.stream().filter(x->x.getCourseversion().equals(coursedata.getCourseversion())).findAny().get();
			Courseversion courseVersion=courseversionList.stream().filter(x->x.getId().getCourseid()==usercourseSubcription.getCourse().getCourseid()).findAny().get();
			
			return courseVersion;
		
	}
	@Override
	public User resetPasswordRequest(String username, String password) {
		Optional<User> user = userRepository.findByusername(username);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
		}
		User userData = user.get();
		userData.setPassword(password);
		userData.setResetpasswordstatus(String.valueOf(STATUS_WAITING));

		userRepository.save(userData);
		LOGGER.info(SAVED_UPDATED_USER, userData.getUsername());

		
		return userData;
	}
	@Override
	public Course registerCourse(CourseRequest request) {
		// TODO Auto-generated method stub
		User user=request.getUser();
		userRepository.save(user);
		LOGGER.info(SAVED_USER, user.getUsername());
		Course course=request.getCourse();
		courseRepository.save(course);
		LOGGER.info("NEW_COURSE");
		return course;
	}
	
	
	
	@Override
	public String resetDetailsOfUser(int userId, String emailOrmobile, String userName) {
		// TODO Auto-generated method stub
          int emailcount = 0;
          int mobilecount=0;
		  Optional<User> user = userRepository.findById(userId);
         if (!user.isPresent()) {
			LOGGER.error(USER_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
		} 
         
			User user1 = user.get();
			
			Set<Userprofile> userprofiles = user1.getUserprofiles();
			
			if(userprofiles==null)
			{
				LOGGER.error(CONTENT_NOT_FOUND);
				throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"Userprofiles must not be null"));
			}
			List<Userprofile> userprofilelist = userprofiles.stream().collect(Collectors.toList());
			List<String> emaillist = new ArrayList<>();
			List<String> mobilenolist = new ArrayList<>();
			if (userprofilelist.isEmpty()) {
				LOGGER.error(CONTENT_NOT_FOUND);
				throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"No email id or mobile number present in the current user profile"));
				
			} 
			else {
				if (!userprofilelist.isEmpty()) {
				if(userName.equals("")||userName.isEmpty())
				{
					return user1.getUniquecode();
					
				}
				if(!userName.equals(""))
				{
					String name = user1.getUsername();
				    for (int i = 0; i < userprofilelist.size(); i++)
						emaillist.add(userprofilelist.get(i).getEmail());
				    for (int i = 0; i < userprofilelist.size(); i++)
						mobilenolist.add(userprofilelist.get(i).getMobile());
				    int flag=0;
				    if (!name.equals(userName)) {
                        
				    	for (int i = 0; i < emaillist.size(); i++) {
							String e = emaillist.get(i);
							
							if (emailOrmobile.equals(e)) {
								emailcount++;
								flag=1;
								
							}
						}
				    	if(emailcount>0 && mobilecount==0 && flag==1)
				    	{
				    		
				            user1.setUsername(userName);
							userRepository.save(user1);
							LOGGER.info(SAVED_UPDATED_USER, user1.getUsername());
							
						}
				    	
				    	for (int i = 0; i < mobilenolist.size(); i++) {
							String m = mobilenolist.get(i);
							
							if (emailOrmobile.equals(m)) {
								mobilecount++;
								flag=2;
							}
						}
				    	if(emailcount==0 && mobilecount>0 && flag==2)
				    	{
				    		user1.setUsername(userName);
							userRepository.save(user1);
							LOGGER.info(SAVED_UPDATED_USER, user1.getUsername());
							
							
							
						}
				    	if(emailcount==0 && mobilecount==0 && flag==0)
				    	{
				    		LOGGER.error(CONTENT_NOT_FOUND);
							throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"Provide proper email id or mobile number"));
						}
						
                       
					}
				    else
				    {  
				    	LOGGER.info(SAVED_UPDATED_USER, user1.getUsername());
					    
				    	return user1.getUniquecode();
				    }
				}
				return user1.getUniquecode();
			}
			else
			{
					LOGGER.error(CONTENT_NOT_FOUND);
					throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"Provide proper emailId or mobile number"));
			}
		}
			
	}

	@Override
	public User checkCustAcctStatus(int userId, String mobileNo, String email, String userName) {
		// TODO Auto-generated method stub
		
		int userflag=0;
		
		Optional<User> user =userRepository.findById(userId);
		
		 if (!user.isPresent()) {
				LOGGER.error(USER_NOT_FOUND);
				throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
			} 
		 User userdata=user.get();
		 Set<Userprofile> userprofiles = userdata.getUserprofiles();
			
			if(userprofiles==null)
			{
				LOGGER.error(CONTENT_NOT_FOUND);
				throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"Userprofiles must not be null"));
			}
			List<Userprofile> userprofilelist = userprofiles.stream().collect(Collectors.toList());
			List<String> emaillist = new ArrayList<>();
			List<String> mobilenolist = new ArrayList<>();
			if (userprofilelist.isEmpty()) {
				LOGGER.error(CONTENT_NOT_FOUND);
				throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"No email id or mobile number present in the current user profile,so cannot retrieve unique code"));
				
			} 
			else
			{
				if (!userprofilelist.isEmpty()) {
					String username=userdata.getUsername();
					if(!userName.equals("")) {
						
					if (username==null) {
						LOGGER.error(CONTENT_NOT_FOUND);
						throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, CONTENT_NOT_FOUND));
					}
					
						for (int i = 0; i < userprofilelist.size(); i++)
							emaillist.add(userprofilelist.get(i).getEmail());
					    for (int i = 0; i < userprofilelist.size(); i++)
							mobilenolist.add(userprofilelist.get(i).getMobile());
					    
					    int emailcount = 0;
					    int mobilecount=0;
					    
					    for (int i = 0; i < emaillist.size(); i++) {
							String e = emaillist.get(i);
							
							if (email.equals(e)) {
								emailcount++;
								
								
							}
						}
					    
					    for (int i = 0; i < mobilenolist.size(); i++) {
							String m = mobilenolist.get(i);
							
							if (mobileNo.equals(m)) {
								mobilecount++;
								
							}
						}
				    	if(emailcount>0 && mobilecount>0 && username.equals(userName))
				    	{
				    		LOGGER.info("Getting user account status");
				    		String getStatus=userdata.getStatus();
				    		if (getStatus==null) {
				    			LOGGER.error(CONTENT_NOT_FOUND);
				    			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, CONTENT_NOT_FOUND));
				    		}
				    		else if (getStatus.equals("A")) {
				    			LOGGER.info("User status is active");
				    			LOGGER.info("Getting User unique code");
				    			LOGGER.info(userdata.getUniquecode());
				    			return userdata;
				    		}
				    		else
				    		{
				    			LOGGER.error("User status is deactive cannot retrieve unique code");
				    			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"User status is deactive cannot retrieve unique code"));
				    		}
				    		
				    		
						}
				    	if(userflag==0  || emailcount==0 || mobilecount==0)
						{
							LOGGER.error(CONTENT_NOT_FOUND);
							throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND,"Provide proper emailid or mobilenumber or username"));
						}
				    	
				}
					
				}
				
			}
		 
		
		return null;
	}
}

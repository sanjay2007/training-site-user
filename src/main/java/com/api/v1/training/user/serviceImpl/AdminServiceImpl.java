package com.api.v1.training.user.serviceImpl;

import static com.api.v1.training.user.util.UserConstantsUtil.STATUS_APPROVED;
import static com.api.v1.training.user.util.UserConstantsUtil.STATUS_WAITING;
import static com.api.v1.training.user.util.UserConstantsUtil.STATUS_OLD;
import static com.api.v1.training.user.util.UserConstantsUtil.USER_NOT_FOUND;
import static com.api.v1.training.user.util.UserConstantsUtil.SAVED_UPDATED_USER_PROFILE;
import static com.api.v1.training.user.util.UserConstantsUtil.SAVED_USER;
import static com.api.v1.training.user.util.UserConstantsUtil.SAVED_UPDATED_USER;
import static com.api.v1.training.user.util.UserConstantsUtil.USER_PROFILE_NOT_FOUND;
import static com.api.v1.training.user.util.UserConstantsUtil.CONTENT_NOT_FOUND;
import static com.api.v1.training.user.util.UserConstantsUtil.*;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.UsercourseSubsciption;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;
import com.api.v1.training.user.exception.TrainingSiteUserApplicationException;
import com.api.v1.training.user.exception.ResourceNotFoundException;
import com.api.v1.training.user.repository.CourseRepository;
import com.api.v1.training.user.repository.UserCourseSubsciptionRepository;
import com.api.v1.training.user.repository.UserProfileRepository;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.response.ErrorData;
import com.api.v1.training.user.service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminServiceImpl.class);

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserProfileRepository userProfileRepository;
	private Random random;

	public AdminServiceImpl() throws NoSuchAlgorithmException {
		random = SecureRandom.getInstanceStrong();
	}

	@Autowired
	UserCourseSubsciptionRepository userCourseSubsciptionRepository;
	@Autowired
	CourseRepository courseRepository;
	

	@Override
	public List<User> getAllUsers() {
		List<User> customer = userRepository.findAll();
		return customer;
	}

	@Override
	public User findById(int userId) {
		User user = userRepository.getOne(userId);
		return user;
	}

	@Override
	public String accountEnableDisable(int userId, String requiredStatus) {

		Optional<User> user = userRepository.findById(userId);

		if (!user.isPresent()) {
			LOGGER.error(USER_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
		}
		String approve_status = "Account status unchanged";
		User user1 = user.get();
        
		String get_Status = user1.getStatus();

		if (get_Status.equals(null)) {
			LOGGER.error(CONTENT_NOT_FOUND);
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, CONTENT_NOT_FOUND));
		}
        if(get_Status.equals("A") && requiredStatus.equals("D"))
        {
        	user1.setStatus(requiredStatus);
        	userRepository.save(user1);
        	approve_status="Account status is disabled";
        }
        if(get_Status.equals("A") && requiredStatus.equals("A"))
        {
        	
        	return approve_status;
        }
        if(get_Status.equals("D") && requiredStatus.equals("A"))
        {
        	user1.setStatus(requiredStatus);
        	userRepository.save(user1);
        	approve_status="Account status is enabled";
        }
        if(get_Status.equals("D") && requiredStatus.equals("D"))
        {
        	
        	return approve_status;
        }
		
		return approve_status;

	}


	

	@Transactional
	@Override
	public User updateUserStatus(String username, String status) {
		Optional<User> user = userRepository.findByusername(username);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
		}
		User userData = user.get();

		Optional<Userprofile> uOptional = userProfileRepository
				.findById(new UserprofileId(userData.getUserid(), userData.getProfileversion()));

		if (!uOptional.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_PROFILE_NOT_FOUND));
		}

		userData.setStatus(status);
		userData.setResetpasswordstatus(status);

		if (status.charAt(0)==STATUS_APPROVED)
			userData.setUniquecode(generateNextUniqueId());
		
		LOGGER.info("Unique Code :{0}",userData.getUniquecode());

		userRepository.save(userData);

		LOGGER.info(SAVED_UPDATED_USER, userData.getUsername());

		Userprofile userprofile = uOptional.get();
		userprofile.setProfileupdatestatus(status);

		userProfileRepository.save(userprofile);

		LOGGER.info(SAVED_UPDATED_USER_PROFILE, userData.getUsername());

		return userData;
	}

	@Override
	public Userprofile updateUserProfileStatus(String username, String status) {

		Optional<User> user = userRepository.findByusername(username);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
		}
		User userData = user.get();
		Optional<Userprofile> newProfileOptional = userProfileRepository.findByuserAndProfileupdatestatus(userData,
				String.valueOf(STATUS_WAITING));
		if (!newProfileOptional.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_PROFILE_NOT_FOUND));
		}

		Optional<Userprofile> oldProfileOptional = userProfileRepository
				.findById(new UserprofileId(userData.getUserid(), userData.getProfileversion()));
		if (!oldProfileOptional.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_PROFILE_NOT_FOUND));
		}
		Userprofile newUserprofile = newProfileOptional.get();
		Userprofile oldUserprofile = oldProfileOptional.get();
		userData.setProfileversion(newUserprofile.getId().getProfileversion());
		userRepository.save(userData);
		LOGGER.info(SAVED_UPDATED_USER, userData.getUsername());

		newUserprofile.setProfileupdatestatus(status);
		if (status.equals(String.valueOf(STATUS_APPROVED))) {
			oldUserprofile.setProfileupdatestatus(String.valueOf(STATUS_OLD));
		}

		List<Userprofile> list = new ArrayList<>();
		list.add(newUserprofile);
		list.add(oldUserprofile);
		userProfileRepository.saveAll(list);
		LOGGER.info(SAVED_UPDATED_USER_PROFILE, userData.getUsername());

		return newUserprofile;
	}

	private String generateNextUniqueId() {

		return String.valueOf(100000000 + random.nextInt(900000000));
	}

	

	@Override
	public User updateUserResetPasswordRequest(String username, String status) {
		Optional<User> user = userRepository.findByusername(username);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
		}
		User userData = user.get();
		userData.setResetpasswordstatus(status);

		userRepository.save(userData);
		LOGGER.info(SAVED_UPDATED_USER, userData.getUsername());

		return userData;
	}

	@Override
	public List<Map<String, Map<String ,String>>> userWiseCourseList() {

		LOGGER.info("Start :Service userWiseCourseList");
		List<UsercourseSubsciption> listUserSubscriptions = userCourseSubsciptionRepository.findAll();
		if(listUserSubscriptions.isEmpty()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, "Not Found"));
		}
		List<Map<String, Map<String,String>>> result = new ArrayList();
		
		Consumer<UsercourseSubsciption> subscriberAction = s->{
			Map<String, Map<String,String>> subscription =new HashMap();
			Map<String,String> courseNameCourseVersion =new HashMap();
			courseNameCourseVersion.put("Course Name :"+courseRepository.getOne((s.getCourse().getCourseid())).getCoursename(), "Course Version : "+courseRepository.getOne((s.getCourse().getCourseid())).getCourseversion());
			subscription.put("UserName : "+s.getUser().getUsername(),courseNameCourseVersion);
			result.add(subscription);
		};
		listUserSubscriptions.forEach(subscriberAction);
		LOGGER.info("Total subscriptions : "+result.size());
		LOGGER.info("End :Service userWiseCourseList");
		
		return result;
	}

	@Override 
	public List<Map<Map<String ,String>,String>> courseWiseUserList() {
		LOGGER.info("Start :Service courseWiseUserList");
		List<UsercourseSubsciption> listCourseWiseUser = userCourseSubsciptionRepository.findAll();
		if(listCourseWiseUser.isEmpty()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, "Not found"));
		}
		List<Map<Map<String ,String>,String>> result = new ArrayList();
		
		Consumer<UsercourseSubsciption> courseSubscriptionAction = s->{
			Map<Map<String,String>,String> subscription =new HashMap();
			Map<String,String> courseNameCourseVersion =new HashMap();
			courseNameCourseVersion.put("Course Name :"+courseRepository.getOne((s.getCourse().getCourseid())).getCoursename(), "Course Version : "+courseRepository.getOne((s.getCourse().getCourseid())).getCourseversion());
			subscription.put(courseNameCourseVersion,"UserName : "+s.getUser().getUsername());
			result.add(subscription);
		};
		
		listCourseWiseUser.forEach(courseSubscriptionAction);
		LOGGER.info("Total subscriptions : "+result.size());
		LOGGER.info("End :Service courseWiseUserList");
		
		return result;
	}
	
	
	
	@Override
	public Map<String, Map<String, String>> userNameWiseCourseList(String userName) {
		Map<String, Map<String, String>> result = new HashMap<>();
		List<UsercourseSubsciption> listUserSubscriptions = userCourseSubsciptionRepository.findAll();
		if (listUserSubscriptions.isEmpty()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, "Not Found"));
		}
		Map<String, String> subscribeCourseList = new HashMap<>();
		Consumer<UsercourseSubsciption> subscriberAction = s -> subscribeCourseList.put(s.getCourse().getCoursename(),
				s.getCourseversion().toString());
		listUserSubscriptions.stream().filter(p -> p.getUser().getUsername().equalsIgnoreCase(userName))
				.collect(Collectors.toList()).forEach(subscriberAction);
		result.put(userName, subscribeCourseList);
		return result;
	}

	@Override
	public Map<String, List<String>> courseNameWiseUserList(String courseName, Integer courseVersion) {
		Map<String, List<String>> result = new HashMap<>();
		List<UsercourseSubsciption> listCourseNameWiseUser = userCourseSubsciptionRepository.findAll();
		if (listCourseNameWiseUser.isEmpty()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, "Not Found"));
		}
		List<String> courseScubscriberList = new ArrayList();
		Consumer<UsercourseSubsciption> courseNameAction = s -> courseScubscriberList.add(s.getUser().getUsername());
		Predicate<UsercourseSubsciption> courseNamePrdicate = p -> p.getCourse().getCoursename().equalsIgnoreCase(courseName);
		Predicate<UsercourseSubsciption> courseVersionPrdicate = p -> p.getCourseversion().equals(courseVersion);
		listCourseNameWiseUser.stream().filter(courseNamePrdicate.and(courseVersionPrdicate)).collect(Collectors.toList()).forEach(courseNameAction);
		if(courseScubscriberList.isEmpty()) {
			throw new ResourceNotFoundException(new ErrorData(HttpStatus.NOT_FOUND, "Not Found"));
		}
		result.put(courseName+" : "+courseVersion, courseScubscriberList);
		return result;
	}	
}

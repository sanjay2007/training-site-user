package com.api.v1.training.user.util;

public class UserConstantsUtil {
	private UserConstantsUtil() {

	}

	public static final char STATUS_WAITING = 'W';
	public static final char STATUS_APPROVED = 'A';
	public static final char STATUS_REJECT = 'R';
	public static final char STATUS_OLD = 'O';

	// URL CONSTANTS
	public static final String TRAININGSITE = "trainingsite";
	public static final String USER = "user";
	public static final String ADMIN = "admin";
	public static final String USERNAME = "username";
	public static final String PROFILE = "profile";

	// LOG MESSAGES
	public static final String REGISTER_USER_REQUEST = "Register new user request started: {} ";
	public static final String REGISTRED_USER_SUCCESSFULLY = "Registered new user successfully: {}";

	public static final String PROFILE_UPDATE_REQUEST = "Update user profile request started : {}";
	public static final String PROFILE_UPDATE_REQUEST_SUCCESSFULLY = "Update user profile requested successfully : {}";
	
	
	public static final String RESET_PASSWORD_REQUEST = "Reset password request : {} ";
	public static final String RESET_PASSWORD_REQUEST_SUCCESSFULLY = "Reset password request done successfully : {}";


	public static final String APPROVE_NEW_USER_REQUEST = "Approve/Reject new user request started : {} ";
	public static final String APPROVED_NEW_USER_REQUEST = "Approved new user successfully : {}";

	public static final String APPROVE_PROFILE_UPDATE_REQUEST = "Approve/Reject updated user profile request started: ";
	public static final String APPROVED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY = "Approved updated user profile successfully";
	
	public static final String APPROVE_RESET_PASSWORD_REQUEST = "Approve/Reject reset passsword request started : {} ";
	public static final String APPROVED_RESET_PASSWORD_REQUEST = "Approved reset password request successfully : {}";

	public static final String SAVED_USER = "Saved new user : {}";
	public static final String SAVED_USER_PROFILE = "Saved new user profile : {}";
	public static final String SAVED_UPDATED_USER = "Saved updated user : {}";
	public static final String SAVED_UPDATED_USER_PROFILE = "Saved updated user profile : {} ";
	public static final String UPDATED_USER_PROFILE_ID = "Updated user with new profile id : {}";
	public static final String NEXT_PROFILE_ID = "Next profile id : {}";

	// RESPONSE MESSAGES
	public static final String REJECTED_NEW_USER_REQUEST = "Rejected new user ";
	public static final String REJECTED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY = "Rejected updated user profile";
	public static final String REJECTED_RESET_PASSWORD_REQUEST = "Rejected reset password request";

	// ERROR MESSAGES
	public static final String USER_NOT_FOUND = "User not found";
	public static final String USER_PROFILE_NOT_FOUND = "User profile not found";
	public static final String CONTENT_NOT_FOUND = "Content not found";

}

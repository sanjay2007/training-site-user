package com.api.v1.training.user.util;

public class UserValidationConstantsUtil {
	private UserValidationConstantsUtil() {

	}

	// REGEX FOR INPUT
	public static final String REGEX_ALPHABET_NUMBER = "[a-zA-Z0-9]+";
	public static final String REGEX_SINGLE_CHAR= "[AR]{1}";
	public static final String REGEX_PASSWORD= "[a-zA-Z0-9]+";

	// ERROR MESSAGES
	public static final String 	MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH="Username should be either alphabet or number or both ";
	public static final String 	MESSAGE_FOR_REGEX_SINGLE_CHAR_MISMATCH="Status should be A  or R ";
	public static final String 	MESSAGE_FOR_REGEX_PASSWORD_MISMATCH="Password should be  either alphabet or number or both (accepts special char #) ";
}

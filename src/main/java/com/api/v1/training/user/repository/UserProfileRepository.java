package com.api.v1.training.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;

public interface UserProfileRepository extends JpaRepository<Userprofile, UserprofileId> {

	//List<Userprofile> findById(UserprofileId userprofileId);

	List<Userprofile> findByuserOrderByIdAsc(User user);

	Optional<Userprofile> findByuserAndProfileupdatestatus(User user, String status);
}

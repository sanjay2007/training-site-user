package com.api.v1.training.user.service;
import java.util.*;
import java.util.List;
import java.util.Optional;
import com.api.v1.training.user.domain.*;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.request.CourseRequest;
import com.api.v1.training.user.request.CustomerRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface CustomerService {

	User register(CustomerRequest request);

	List<User> getAllUsers();

	Optional<User> findById(int userId);

	Userprofile addUserProfile(String username, Userprofile userprofile);

	User resetPasswordRequest(String username, String password);

	public Set<UsercourseSubsciption> retrieveCourseSubcriptionByUser(String username);

	Courseversion retrieveSubscribedCourseContent(int userId, int courseId);

	 Course registerCourse(CourseRequest request);

	 public String resetDetailsOfUser(int userId, String emailOrmobile, String userName);

	User checkCustAcctStatus(int userId, String mobileNo, String email, String userName);

     

}

package com.api.v1.training.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.User;

public interface CourseRepository extends JpaRepository<Course, Integer> {

}

package com.api.v1.training.user.exception;

import javax.servlet.ServletException;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.api.v1.training.user.response.ErrorData;

@ControllerAdvice
public class UserExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserExceptionHandler.class);

	@ExceptionHandler({ Exception.class, ServletException.class })
	public ResponseEntity<Object> genericException(Exception ex) {
		ErrorData errorData = createError(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR, ex);
		LOGGER.error(ex.getLocalizedMessage(), errorData.getStatus(), errorData.getTimestamp());
		return new ResponseEntity<>(errorData, errorData.getStatus());
	}

	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> invalidRequestException(Exception ex) {
		ex.printStackTrace();
		ErrorData errorData = createError(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST, ex);
		LOGGER.error(ex.getLocalizedMessage(), errorData.getStatus(), errorData.getTimestamp());
		return new ResponseEntity<>(errorData, errorData.getStatus());
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<Object> resourceEntityNotFound(ResourceNotFoundException ex) {
		ErrorData errorData = ex.getData();
		errorData.setStatus(HttpStatus.NOT_FOUND);
		LOGGER.error(ex.getLocalizedMessage(), errorData.getStatus(), errorData.getTimestamp());
		return new ResponseEntity<>(errorData, errorData.getStatus());
	}

	private ErrorData createError(String msg, HttpStatus status, Exception e) {
		ErrorData errorData = new ErrorData(status);
		errorData.setMessage(msg);
		errorData.setDebugMessage(e.getMessage());
		return errorData;
	}

}

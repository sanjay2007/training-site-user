package com.api.v1.training.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.v1.training.user.domain.UsercourseSubsciption;
import com.api.v1.training.user.domain.UsercourseSubsciptionId;

public interface UserCourseSubsciptionRepository extends JpaRepository<UsercourseSubsciption, UsercourseSubsciptionId> {
	
}

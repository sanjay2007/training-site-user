package com.api.v1.training.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.v1.training.user.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}

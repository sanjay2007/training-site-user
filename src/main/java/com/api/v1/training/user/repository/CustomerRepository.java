package com.api.v1.training.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.v1.training.user.domain.User;

@Repository
public interface CustomerRepository extends JpaRepository<User, Integer>{

  
}
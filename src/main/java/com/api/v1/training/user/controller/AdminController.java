package com.api.v1.training.user.controller;

import java.util.List;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.service.AdminService;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.REGEX_ALPHABET_NUMBER;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH;

@RestController
@RequestMapping("/trainingsite/admin")
public class AdminController {
	
	Logger logger = LoggerFactory.getLogger(AdminController.class);
   
	
	@Autowired
	private AdminService adminService;

	
	  //Report : User Wise Course List
	  
	  @GetMapping(path ="/userWiseCourseList" ) public
	  ResponseEntity<List<Map<String,Map<String,String>>>> userWiseCourseList() {
	  logger.info("Start : userWiseCourseList controller ");
	  List<Map<String,Map<String,String>>> listUserWiseCourse =
	  adminService.userWiseCourseList();
	  logger.info("End : userWiseCourseList controller "); return
	  ResponseEntity.status(HttpStatus.OK).body(listUserWiseCourse); }
	  
	  //Report : Course Wise User List
	  
	  @GetMapping(path = "/courseWiseUserList" ) public
	  ResponseEntity<List<Map<Map<String ,String>,String>>> courseWiseUserList() {
	  List<Map<Map<String ,String>,String>> listCourseWiseUsers =
	  adminService.courseWiseUserList(); return
	  ResponseEntity.status(HttpStatus.OK).body(listCourseWiseUsers); }
	 
	
	
	// Report : UserName Wise Course List
		@GetMapping(path = "/userNameWiseCourseList/{userName}")
		public ResponseEntity<Map<String, Map<String, String>>> userNameWiseCourseList(
				@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message = MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value = "userName") String userName) {
			logger.info("Start : userWiseCourseList controller ");
			Map<String, Map<String, String>> mapUserNameWiseCourse = adminService.userNameWiseCourseList(userName);
			logger.info("End : userWiseCourseList controller ");
			return ResponseEntity.status(HttpStatus.OK).body(mapUserNameWiseCourse);
		}

		
		  // Report : CourseName-CourseVersion Wise User List
		  @GetMapping(path = "/courseNameWiseUserList/{courseName}/{courseVersion}") public
		  ResponseEntity<Map<String,List<String>>> courseNameWiseUserList(@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message =  MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value ="courseName") String courseName,
				  @Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message =  MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value ="courseVersion") Integer courseVersion) { 
			  Map<String,List<String>> mapCourseNameWiseUsers = adminService.courseNameWiseUserList(courseName,courseVersion);
		  return ResponseEntity.status(HttpStatus.OK).body(mapCourseNameWiseUsers); 
		  }
   
	
  @RequestMapping(value = { "/users" }, method = RequestMethod.GET)
   	public List<User> getAllUserList() {
       	return adminService.getAllUsers();
   	}
	
   @RequestMapping(value = { "/{id}" }, method = RequestMethod.GET)
  	public User getUniqueUser(@PathVariable int id) {
      	return adminService.findById(id);
  	}
 	
	
	
   @RequestMapping(value = { "/enabledisableAccount/{userId}/{requiredStatus}" }, method = RequestMethod.PUT)
	public String enableDisableAccount(@PathVariable(value = "userId") int userId,@PathVariable(value = "requiredStatus") String requiredStatus) {
  	// TODO return type from string to list
  	logger.info("Update Account status whether it is changed or not");
  	return adminService.accountEnableDisable(userId,requiredStatus);
	}
    
}
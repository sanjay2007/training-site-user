package com.api.v1.training.user.request;

import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.User;

import lombok.Data;

@Data
public class CourseRequest {

	private Course course;
	private User user;
	
	public CourseRequest()
	{
		
	}

	public CourseRequest(Course course, User user) {
		super();
		this.course = course;
		this.user = user;
	}


	

	
}

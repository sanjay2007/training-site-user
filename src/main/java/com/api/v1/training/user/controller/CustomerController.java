package com.api.v1.training.user.controller;
import static com.api.v1.training.user.util.UserConstantsUtil.APPROVED_NEW_USER_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.APPROVED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY;
import static com.api.v1.training.user.util.UserConstantsUtil.APPROVE_NEW_USER_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.APPROVE_PROFILE_UPDATE_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.PROFILE_UPDATE_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.PROFILE_UPDATE_REQUEST_SUCCESSFULLY;
import static com.api.v1.training.user.util.UserConstantsUtil.REGISTER_USER_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.REGISTRED_USER_SUCCESSFULLY;
import static com.api.v1.training.user.util.UserConstantsUtil.REJECTED_NEW_USER_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.REJECTED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY;
import static com.api.v1.training.user.util.UserConstantsUtil.STATUS_APPROVED;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.REGEX_ALPHABET_NUMBER;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.REGEX_SINGLE_CHAR;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.MESSAGE_FOR_REGEX_SINGLE_CHAR_MISMATCH;

import static com.api.v1.training.user.util.UserValidationConstantsUtil.REGEX_PASSWORD;
import static com.api.v1.training.user.util.UserValidationConstantsUtil.MESSAGE_FOR_REGEX_PASSWORD_MISMATCH;
import static com.api.v1.training.user.util.UserConstantsUtil.APPROVED_RESET_PASSWORD_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.APPROVE_RESET_PASSWORD_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.REJECTED_RESET_PASSWORD_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.RESET_PASSWORD_REQUEST;
import static com.api.v1.training.user.util.UserConstantsUtil.RESET_PASSWORD_REQUEST_SUCCESSFULLY;

import java.util.Optional;
import java.util.*;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.response.ResponseData;
import com.api.v1.training.user.service.AdminService;
import com.api.v1.training.user.service.CustomerService;
import com.api.v1.training.user.domain.*;

@Validated
@RestController
@RequestMapping("/trainingsite/user")
public class CustomerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	

	@Autowired
	private CustomerService customerService;
	@Autowired
	private AdminService adminService;

	@GetMapping(value = "/checkCustAcctStatus/{userId}/{mobileNo}/{email}/{userName}")
	public ResponseEntity<User> checkCustAcctStatus(@PathVariable(value = "userId") int userId,@PathVariable(value = "mobileNo") String mobileNo,@PathVariable(value = "email") String email,@PathVariable(value = "userName") String userName) {
		LOGGER.info("Checking User account status by providing mobile number,email and usernmae");
		User userdata=customerService.checkCustAcctStatus(userId,mobileNo,email,userName);
		LOGGER.info("Response recieved");
		return ResponseEntity.ok().body(userdata);
	}

	@PostMapping()
	public ResponseEntity<ResponseData> registerCustomer(@Valid @RequestBody CustomerRequest request) {
		LOGGER.info(REGISTER_USER_REQUEST,request.getUser().getUsername());
		customerService.register(request);
		LOGGER.info(REGISTRED_USER_SUCCESSFULLY ,request.getUser().getUsername());
		
		return ResponseEntity.ok().body(new ResponseData(HttpStatus.CREATED, REGISTRED_USER_SUCCESSFULLY));
	}
	@PostMapping(value = "/{username}/profile")
	public ResponseData addUpdatedCustomerProfile(@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message = MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value = "username") String username,
		@Valid	@RequestBody Userprofile profile) {
		LOGGER.info(PROFILE_UPDATE_REQUEST,username);
		customerService.addUserProfile(username, profile);
		LOGGER.info(PROFILE_UPDATE_REQUEST_SUCCESSFULLY,username);
		return new ResponseData(HttpStatus.OK, PROFILE_UPDATE_REQUEST_SUCCESSFULLY);
		
	}
	
	@PutMapping(value = "/{username}")
	public ResponseData approveOrRejectNewUser(@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message = MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value = "username") String username,
			@Valid @Pattern(regexp = REGEX_SINGLE_CHAR, message = MESSAGE_FOR_REGEX_SINGLE_CHAR_MISMATCH)	@RequestParam(value = "status") String status) {
		LOGGER.info(APPROVE_NEW_USER_REQUEST,username);
		adminService.updateUserStatus(username, status);
		LOGGER.info(APPROVED_NEW_USER_REQUEST,username);
		return new ResponseData(HttpStatus.OK,
				((status.charAt(0) == STATUS_APPROVED) ? APPROVED_NEW_USER_REQUEST : REJECTED_NEW_USER_REQUEST));
		
	}

	@PutMapping(value = "/{username}/profile")
	public ResponseData approveOrRejectUserProfile(@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message = MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value = "username") String username,
			@Valid @Pattern(regexp = REGEX_SINGLE_CHAR, message = MESSAGE_FOR_REGEX_SINGLE_CHAR_MISMATCH) @RequestParam(value = "status") String status) {
		LOGGER.info(APPROVE_PROFILE_UPDATE_REQUEST,username);
		adminService.updateUserProfileStatus(username, status);
		LOGGER.info(APPROVED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY,username);
		return new ResponseData(HttpStatus.OK,
				((status.charAt(0) == STATUS_APPROVED) ? APPROVED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY : REJECTED_PROFILE_UPDATE_REQUEST_SUCCESSFULLY));
	}
	
	@PutMapping(value = "/{username}/resetPasswordRequest")
	public ResponseData resetPasswordRequest(@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message = MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value = "username") String username,
		 @RequestBody String password) {
		LOGGER.info(RESET_PASSWORD_REQUEST,username);
		customerService.resetPasswordRequest(username, password.trim());
		LOGGER.info(RESET_PASSWORD_REQUEST_SUCCESSFULLY,username);
		return new ResponseData(HttpStatus.OK, RESET_PASSWORD_REQUEST_SUCCESSFULLY);
		
	}
	
	@PutMapping(value = "/{username}/resetPassword")
	public ResponseData approveOrRejectResetPassword(@Valid @Pattern(regexp = REGEX_ALPHABET_NUMBER, message = MESSAGE_FOR_REGEX_ALPHABET_NUMBER_MISMATCH) @PathVariable(value = "username") String username,
			@Valid @Pattern(regexp = REGEX_SINGLE_CHAR, message = MESSAGE_FOR_REGEX_SINGLE_CHAR_MISMATCH)	@RequestParam(value = "status") String status){
		LOGGER.info(APPROVE_RESET_PASSWORD_REQUEST,username);
		adminService.updateUserResetPasswordRequest(username, status);
		LOGGER.info(APPROVED_RESET_PASSWORD_REQUEST,username);
		return new ResponseData(HttpStatus.OK,
				((status.charAt(0) == STATUS_APPROVED) ? APPROVED_RESET_PASSWORD_REQUEST : REJECTED_RESET_PASSWORD_REQUEST));
	}

	@GetMapping(value = "/{username}/course")
	public Set<UsercourseSubsciption> retrieveCourseSubcriptionByUser(@PathVariable String username) {
		LOGGER.info("Check whether user has any subcription to courses or not");
		return customerService.retrieveCourseSubcriptionByUser(username);
	}
	
	@GetMapping(value = "/{userId}/course/{courseId}")
	public Courseversion retrieveSubscribedCourseContent(@PathVariable int userId,@PathVariable int courseId) {
		LOGGER.info("Checking course content for subscribed courses");
		return customerService.retrieveSubscribedCourseContent(userId,courseId);
	}
	
	
	@RequestMapping(value = { "/resetDetailsOfUser/{userId}/{emailOrmobile}/{userName}"}, method = RequestMethod.PUT)
	public String resetDetailsOfUser(@PathVariable(value = "userId") int userId,@PathVariable(value = "emailOrmobile") String emailOrmobile,@PathVariable(value = "userName") String userName) {
   	// TODO return type from string to list
		LOGGER.info("Reset username if user forgets by providing emailId/mobile");
   	return customerService.resetDetailsOfUser(userId,emailOrmobile,userName);
	}
}
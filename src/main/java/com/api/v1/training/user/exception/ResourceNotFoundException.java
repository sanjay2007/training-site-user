package com.api.v1.training.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.api.v1.training.user.response.ErrorData;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends TrainingSiteUserApplicationException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException(ErrorData data) {
		super(data);
		
	}


//	public ResourceNotFoundException(String exception) {
//		super(exception);
//	}

}

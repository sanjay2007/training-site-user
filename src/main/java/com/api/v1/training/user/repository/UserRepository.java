package com.api.v1.training.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.v1.training.user.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
 Optional<User>  findByusername(String username);
}

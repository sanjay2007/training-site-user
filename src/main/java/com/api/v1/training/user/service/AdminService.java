package com.api.v1.training.user.service;

import java.util.List;
import java.util.Map;

import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;

public interface AdminService {

	List<User> getAllUsers();

	User findById(int userId);

	String accountEnableDisable(int userId, String requiredStatus);

	User updateUserStatus(String username, String status);

	Userprofile updateUserProfileStatus(String username, String status);

	User updateUserResetPasswordRequest(String username, String status);

	
	List<Map<String, Map<String, String>>> userWiseCourseList();
	
	List<Map<Map<String ,String>,String>> courseWiseUserList();
	
	Map<String,Map<String ,String>> userNameWiseCourseList(String userName);

	Map<String, List<String>> courseNameWiseUserList(String courseName, Integer courseVersion);
	
}

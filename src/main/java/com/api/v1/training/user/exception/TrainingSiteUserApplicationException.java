package com.api.v1.training.user.exception;

import com.api.v1.training.user.response.ErrorData;

import lombok.Data;

@Data
public class TrainingSiteUserApplicationException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	private final ErrorData data;

	
	public TrainingSiteUserApplicationException(ErrorData data) {
		super();
		this.data=data;
	}
	
	
	

}

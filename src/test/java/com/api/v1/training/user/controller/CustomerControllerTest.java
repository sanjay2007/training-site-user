package com.api.v1.training.user.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.api.v1.training.user.controller.CustomerController;
import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.Courseversion;
import com.api.v1.training.user.domain.CourseversionId;
import com.api.v1.training.user.domain.Role;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.UsercourseSubsciption;
import com.api.v1.training.user.domain.UsercourseSubsciptionId;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.request.CourseRequest;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.response.ResponseData;
import com.api.v1.training.user.service.AdminService;
import com.api.v1.training.user.service.CustomerService;

import org.junit.platform.runner.JUnitPlatform;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class CustomerControllerTest {
	@InjectMocks
	private CustomerController customerController;

	@Mock
	private UserRepository userRepository;

	@Mock
	private CustomerService customerService;
	@Mock
	private AdminService adminService;
	private static CustomerRequest customerRequest;
	private static CustomerRequest customerRequest1;
	private static CourseRequest courseRequest;

	@BeforeAll
	public static void setup() {
		Course course1=new Course(3,"java",15,"A",null,null);
		Role role1 = new Role(1);
		User user1 = new User(1, role1, 1, "A", "smita", "123456", "A","sam", null, null);
		UsercourseSubsciptionId usersubcriptionid=new UsercourseSubsciptionId(1,3);
		UsercourseSubsciption usersubcription=new UsercourseSubsciption(usersubcriptionid,course1,user1,15);
		Set<UsercourseSubsciption> usersubcriptionList=new HashSet<>();
		usersubcriptionList.add(usersubcription);
		
		Role role = new Role(1);
		User user = new User(1, role, 1, "A", "smita", "123456", "A","sam",usersubcriptionList, null);
		UserprofileId id = new UserprofileId(1, 1);
		Userprofile userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		customerRequest = new CustomerRequest(user, userprofile);
		
		CourseversionId courseVersionId=new CourseversionId(3,15);
		Courseversion courseVersion=new Courseversion(courseVersionId,course1,"java","online",300.0,"15","Java Programming","courseversioncol");
		Set<Courseversion> courseVersionList=new HashSet<>();
		courseVersionList.add(courseVersion);
		Course course=new Course(3,"java",15,"A",courseVersionList,null);
		courseRequest=new CourseRequest(course,user);

	}

	@Test
	void testRegisterCustomerSuccess() {

		when(customerService.register(any(CustomerRequest.class))).thenReturn(new User());

		ResponseEntity<ResponseData> response = customerController.registerCustomer(customerRequest);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

	}

	@Test
	void testAddUpdatedCustomerProfileSuccess() {

		when(customerService.addUserProfile(any(String.class), any(Userprofile.class))).thenReturn(new Userprofile());

		ResponseData response = customerController.addUpdatedCustomerProfile("sam", customerRequest.getUserprofile());

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);

	}

	@Test
	void testApproveOrRejectNewUserSuccess() {

		when(adminService.updateUserStatus(any(String.class), any(String.class))).thenReturn(new User());

		ResponseData response = customerController.approveOrRejectNewUser("sam", "A");

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);
		
		 response = customerController.approveOrRejectNewUser("sam", "R");

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);
		
		verify(adminService, times(1)).updateUserStatus("sam","R");

	}
	
	@Test
	void testapproveOrRejectUserProfileSuccess() {

		when(adminService.updateUserProfileStatus(any(String.class), any(String.class))).thenReturn(new Userprofile());

		ResponseData response = customerController.approveOrRejectUserProfile("sam", "A");

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);
	
		
		 response = customerController.approveOrRejectUserProfile("sam", "R");

		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK);

	}

	@Test
	void checkRetrieveSubscribedCourseContentTest()  {
		
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		 
		  User userdata=courseRequest.getUser();
		  Course coursedata=courseRequest.getCourse();
		  UsercourseSubsciption usercourseSubcription=userdata.getUsercourseSubsciptions().stream().filter(x->x.getCourseversion()==(coursedata.getCourseversion())).findAny().get();
		  Courseversion courseversion=coursedata.getCourseversions().stream().filter(x->x.getId().getCourseid()==usercourseSubcription.getCourse().getCourseid()).findAny().get();
		 when(customerService.retrieveSubscribedCourseContent(userdata.getUserid(),coursedata.getCourseid())).thenReturn(courseversion);
		
		 Courseversion courseversion1=customerController.retrieveSubscribedCourseContent(1,3);
		 assertThat(courseversion1.getContents()).isEqualTo("Java Programming");
		
		
	}
	
	@Test
	 void checkResetDetailsForUserTest()
	{
		Role role = new Role(1);
		User user = new User(1, role, 1, "D", "smita", "123456", "A", "sam", null, null);
		UserprofileId id = new UserprofileId(1, 1);
		Userprofile userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
        customerRequest1 = new CustomerRequest(user, userprofile);
        
        MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		User user1=customerRequest1.getUser();
		Userprofile userprofile1=customerRequest1.getUserprofile();
		String emailOrmobile=userprofile1.getMobile();
		int userid=user1.getUserid();
		
		when(customerService.resetDetailsOfUser(userid, emailOrmobile,"bishal")).thenReturn(user1.getUniquecode());
		String result=customerController.resetDetailsOfUser(userid, emailOrmobile,"bishal");
		System.out.println(result);
		assertThat(result).isEqualTo(user1.getUniquecode());
	}
	
	@Test
	 void checkCustAcctStatusTest()
	{
		
		Role role = new Role(1);
		User user = new User(1, role, 1, "A", "smita", "123456", "A", "sam", null, null);
		UserprofileId id = new UserprofileId(1, 1);
		Userprofile userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		
		Set<Userprofile> userprofilelist=new HashSet<>();
		userprofilelist.add(userprofile);
		
		Role role1 = new Role(2);
		User user1 = new User(2, role1, 1, "A", "smita", "123456", "A", "sam", null,userprofilelist);
		UserprofileId id1 = new UserprofileId(2, 2);
		Userprofile userprofile1 = new Userprofile(id1, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		
        customerRequest1 = new CustomerRequest(user1, userprofile1);
        
        MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		User userdata=customerRequest1.getUser();
	
	  when(customerService.checkCustAcctStatus(2,"8237550368","smita.khot25@gmail.com","sam")).thenReturn(userdata);
	   
	   ResponseEntity<User> response =customerController.checkCustAcctStatus(2,"8237550368","smita.khot25@gmail.com","sam");
	   assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	  
	}


	@AfterAll
	public static void clear() {
		customerRequest = null;
	}

}

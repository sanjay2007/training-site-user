package com.api.v1.training.user.controller;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.api.v1.training.user.domain.Role;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;
import com.api.v1.training.user.repository.UserProfileRepository;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.service.AdminService;
import com.api.v1.training.user.service.CustomerService;

import org.junit.platform.runner.JUnitPlatform;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class AdminControllerTest {

	@InjectMocks
	private AdminController adminController;
	@Mock
	private AdminService adminService;
	@Mock
	private CustomerService customerService;

	private static CustomerRequest customerRequest;

	@BeforeAll
	public static void setUp() {
		Role role = new Role(1);
		User user = new User(1, role, 1, "A", "smita", "123456", "A", "sam", null, null);
		UserprofileId id = new UserprofileId(1, 1);
		Userprofile userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");

		customerRequest = new CustomerRequest(user, userprofile);
		System.out.println("Testing started");

	}

	 @Test
		public void checkEnableDisableAccountForUserTest() throws Exception {
			MockHttpServletRequest request = new MockHttpServletRequest();
			RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
			User user1=customerRequest.getUser();
			int id=user1.getUserid();
			when(adminService.accountEnableDisable(id,"D")).thenReturn("Account status is disabled");
			String result=adminController.enableDisableAccount(id,"D");
			System.out.println(result);
			assertThat(result).isEqualTo("Account status is disabled");
		}

	@AfterAll
	public static void clear() {

		System.out.println("Testing done");
	}

}

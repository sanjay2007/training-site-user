package com.api.v1.training.user.serviceimpl;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.api.v1.training.user.domain.Course;
import com.api.v1.training.user.domain.Courseversion;
import com.api.v1.training.user.domain.CourseversionId;
import com.api.v1.training.user.domain.Role;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.UsercourseSubsciption;
import com.api.v1.training.user.domain.UsercourseSubsciptionId;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;
import com.api.v1.training.user.repository.CourseRepository;
import com.api.v1.training.user.repository.UserProfileRepository;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.request.CourseRequest;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.serviceImpl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class CustomerServiceTest {

	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;

	@Mock
	private UserRepository userRepository;
	@Mock
	private CourseRepository courseRepository;
	@Mock
	private UserProfileRepository userProfileRepository;
	private static CustomerRequest customerRequest;
	private static CustomerRequest customerRequest1;
	private static User user;
	private static Userprofile userprofile;
	private static List<Userprofile> userProfileList;

	private static CourseRequest courseRequest;

	@BeforeAll
	public static void setup() {
		Course course1 = new Course(3, "java", 15, "A", null, null);
		Role role1 = new Role(1);
		User user1 = new User(1, role1, 1, "A", "smita", "123456", "A", "sam", null, null);
		UsercourseSubsciptionId usersubcriptionid = new UsercourseSubsciptionId(1, 3);
		UsercourseSubsciption usersubcription = new UsercourseSubsciption(usersubcriptionid, course1, user1, 15);
		Set<UsercourseSubsciption> usersubcriptionList = new HashSet<>();
		usersubcriptionList.add(usersubcription);

		Role role = new Role(1);
		user = new User(1, role, 1, "A", "smita", "123456", "A", "sam", usersubcriptionList, null);
		UserprofileId id = new UserprofileId(1, 1);
		userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		customerRequest = new CustomerRequest(user, userprofile);

		user = customerRequest.getUser();
		userprofile = customerRequest.getUserprofile();
		userProfileList = new ArrayList<Userprofile>();
		userProfileList.add(userprofile);

		CourseversionId courseVersionId = new CourseversionId(3, 15);
		Courseversion courseVersion = new Courseversion(courseVersionId, course1, "java", "online", 300.0, "15",
				"Java Programming", "courseversioncol");
		Set<Courseversion> courseVersionList = new HashSet<>();
		courseVersionList.add(courseVersion);
		Course course = new Course(3, "java", 15, "A", courseVersionList, null);
		courseRequest = new CourseRequest(course, user);

	}

	@Test
	void testRegisterSuccess() {
		// MOCK CALLs
		when(userRepository.save(user)).thenReturn(user);
		when(userProfileRepository.save(userprofile)).thenReturn(userprofile);
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));

		when(userProfileRepository.findByuserOrderByIdAsc(user)).thenReturn(userProfileList);

		// ACTUAL METHOD CALL
		User userdata = customerServiceImpl.register(customerRequest);

		// TEST RESULT
		assertEquals("sam", userdata.getUsername());
		verify(userRepository, times(2)).save(user);
		verify(userProfileRepository, times(1)).save(userprofile);

	}

	@Test
	void testaddUserProfileSuccess() {
		// DUMMY DATA
		UserprofileId id = new UserprofileId(1, 2);
		Userprofile newuserprofile = new Userprofile(id, user);
		newuserprofile.setLastname("Suryawanshi");

		// MOCK CALLs
		when(userRepository.findByusername(user.getUsername())).thenReturn(Optional.of(user));
		when(userProfileRepository.findByuserAndProfileupdatestatus(user, "A")).thenReturn(Optional.of(userprofile));
		when(userProfileRepository.save(newuserprofile)).thenReturn(newuserprofile);
		when(userProfileRepository.findByuserOrderByIdAsc(user)).thenReturn(userProfileList);
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));

		// ACTUAL METHOD CALL
		Userprofile profile = customerServiceImpl.addUserProfile(user.getUsername(), newuserprofile);

		// TEST RESULT
		assertEquals("smita", profile.getFirstname());
		assertEquals("Suryawanshi", profile.getLastname());
		assertEquals("W", profile.getProfileupdatestatus());

		verify(userProfileRepository, times(1)).save(newuserprofile);

	}

	@Test
	void testResetPasswordRequestSuccess() {
		// MOCK CALLs
		when(userRepository.findByusername(user.getUsername())).thenReturn(Optional.of(user));
		when(userRepository.save(user)).thenReturn(user);

		// ACTUAL METHOD CALL
		customerServiceImpl.resetPasswordRequest(user.getUsername(), "sami123");

		// TEST RESULT
		assertEquals("W", user.getResetpasswordstatus());
		verify(userRepository, times(1)).save(user);
	}

	@Test
	void checkRetrieveSubscribedCourseContentTest() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		User user = courseRequest.getUser();
		when(userRepository.save(user)).thenReturn(user);
		when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));
		
		
		Course course = courseRequest.getCourse();
		when(courseRepository.save(course)).thenReturn(course);
		when(courseRepository.findById(course.getCourseid())).thenReturn(Optional.of(course));
		customerServiceImpl.registerCourse(courseRequest);
       
		assertEquals("sam", user.getUsername());
		Courseversion courseversion = customerServiceImpl.retrieveSubscribedCourseContent(1, 3);
       
		assertEquals("Java Programming", courseversion.getContents());
	}

	
	@Test
	 void checkResetDetailsForUserTest()
	{
		Role role = new Role(1);
		User user = new User(1, role, 1, "D", "smita", "123456", "A", "sam", null, null);
		UserprofileId id = new UserprofileId(1, 1);
		Userprofile userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		
		Set<Userprofile> userProfileSet=new HashSet<>();
        userProfileSet.add(userprofile);
		
		Role role2= new Role(2);
		User user2= new User(2, role2, 2, "D", "smita1", "123456", "A", "sam", null,userProfileSet);
		UserprofileId id1 = new UserprofileId(2, 2);
		Userprofile userprofile2 = new Userprofile(id1, user2, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
	    customerRequest1 = new CustomerRequest(user2, userprofile2);
        
        MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		User user1=customerRequest1.getUser();
		Userprofile userprofile1=customerRequest1.getUserprofile();
		
		// MOCK CALLS
		when(userRepository.save(user1)).thenReturn(user1);
		when(userRepository.findById(user1.getUserid())).thenReturn(Optional.of(user1));
       

	     String result=customerServiceImpl.resetDetailsOfUser(user1.getUserid(),userprofile1.getMobile(),"bishal");
		 assertThat(result).isEqualTo(user1.getUniquecode());
		
	}
	
	@Test
	 void checkCustAcctStatusTest()
	{
		
		Role role = new Role(1);
		User user = new User(1, role, 1, "A", "smita", "123456", "A", "sam", null, null);
		UserprofileId id = new UserprofileId(1, 1);
		Userprofile userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		
		Set<Userprofile> userprofilelist=new HashSet<>();
		userprofilelist.add(userprofile);
		
		Role role1 = new Role(2);
		User user1 = new User(2, role1, 1, "A", "smita", "123456", "A", "sam", null,userprofilelist);
		UserprofileId id1 = new UserprofileId(2, 2);
		Userprofile userprofile1 = new Userprofile(id1, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		
        customerRequest1 = new CustomerRequest(user1, userprofile1);
        
        MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		User userdata=customerRequest1.getUser();
		
		
		// MOCK CALLs
		when(userRepository.findById(userdata.getUserid())).thenReturn(Optional.of(userdata));

		// ACTUAL METHOD CALL
		User userrecord= customerServiceImpl.checkCustAcctStatus(2,"8237550368","smita.khot25@gmail.com","sam");
		
		// TEST RESULT
		assertEquals("123456", userrecord.getUniquecode());
	}

	@AfterAll
	static void clear() {
		customerRequest = null;
	}
}

package com.api.v1.training.user.serviceimpl;

import static com.api.v1.training.user.util.UserConstantsUtil.STATUS_WAITING;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.api.v1.training.user.domain.Role;
import com.api.v1.training.user.domain.User;
import com.api.v1.training.user.domain.Userprofile;
import com.api.v1.training.user.domain.UserprofileId;
import com.api.v1.training.user.repository.UserProfileRepository;
import com.api.v1.training.user.repository.UserRepository;
import com.api.v1.training.user.request.CustomerRequest;
import com.api.v1.training.user.serviceImpl.AdminServiceImpl;
import com.api.v1.training.user.serviceImpl.CustomerServiceImpl;
import java.util.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class AdminServiceTest {

	@InjectMocks
	private AdminServiceImpl adminServiceImpl;
	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;

	@Mock
	private UserRepository userRepository;

	@Mock
	private UserProfileRepository userProfileRepository;

	private static CustomerRequest customerRequest;
	private static CustomerRequest customerRequest1;
	private static User user;
	private static Userprofile userprofile;
	private static List<Userprofile> userProfileList;

	@BeforeAll
	public static void setup() {
		Role role = new Role(1);
		user = new User(1, role, 1, "A", "smita", "", "A", "sam", null, null);
		UserprofileId id = new UserprofileId(1, 1);
		userprofile = new Userprofile(id, user, "smita", "khot", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "A");
		customerRequest = new CustomerRequest(user, userprofile);
		user = customerRequest.getUser();
		userprofile = customerRequest.getUserprofile();
		userProfileList = new ArrayList<Userprofile>();
		userProfileList.add(userprofile);

	}

	@Test
	void testUpdateUserStatusSuccess() {
		// DUMMY DATA
		user.setStatus("W");
		userprofile.setProfileupdatestatus("W");

		// MOCK CALLs
		when(userRepository.findByusername(user.getUsername())).thenReturn(Optional.of(user));
		when(userProfileRepository.findById(new UserprofileId(user.getUserid(), user.getProfileversion())))
				.thenReturn(Optional.of(userprofile));
		when(userRepository.save(user)).thenReturn(user);
		when(userProfileRepository.save(userprofile)).thenReturn(userprofile);

		// ACTUAL METHOD CALL
		adminServiceImpl.updateUserStatus(user.getUsername(), "A");

		
		// TEST RESULT
		assertEquals("A", user.getStatus());
		assertEquals("A", userprofile.getProfileupdatestatus());
		assertThat(user.getUniquecode()).isNotBlank();

		verify(userRepository, times(1)).save(user);
		verify(userProfileRepository, times(1)).save(userprofile);

	}

	@Test
	void testUpdateUserProfileStatusSuccess() {
		// DUMMY DATA
		UserprofileId id = new UserprofileId(1, 2);
		Userprofile newuserprofile = new Userprofile(id, user, "smita", "suryawanshi", "Pune", "Maharshtra", "India",
				"smita.khot25@gmail.com", "8237550368", "W");

		List<Userprofile> userProfileList = new ArrayList<Userprofile>();
		userProfileList.add(newuserprofile);
		userProfileList.add(userprofile);

		// MOCK CALL
		when(userRepository.findByusername(user.getUsername())).thenReturn(Optional.of(user));
		when(userProfileRepository.findById(new UserprofileId(user.getUserid(), user.getProfileversion())))
				.thenReturn(Optional.of(userprofile));
		when(userProfileRepository.findByuserAndProfileupdatestatus(user, String.valueOf(STATUS_WAITING)))
				.thenReturn(Optional.of(newuserprofile));

		when(userRepository.save(user)).thenReturn(user);
		when(userProfileRepository.saveAll(userProfileList)).thenReturn(userProfileList);

		// ACTUAL METHOD CALL
		adminServiceImpl.updateUserProfileStatus(user.getUsername(), "A");

		// TEST RESULT
		assertEquals(2, user.getProfileversion());
		assertEquals("A", newuserprofile.getProfileupdatestatus());
		assertEquals("O", userprofile.getProfileupdatestatus());

		verify(userRepository, times(1)).save(user);
		verify(userProfileRepository, times(1)).saveAll(userProfileList);
	}

	@Test
	void testUpdateUserResetPasswordRequestSuccess() {
		// DUMMY DATA
		user.setResetpasswordstatus("W");

		// MOCK CALL
		when(userRepository.findByusername(user.getUsername())).thenReturn(Optional.of(user));
		when(userRepository.save(user)).thenReturn(user);

		// ACTUAL METHOD CALL
		adminServiceImpl.updateUserResetPasswordRequest(user.getUsername(), "A");
		
		// TEST RESULT
		assertEquals("A", user.getResetpasswordstatus());
		verify(userRepository, times(1)).save(user);	

	}

	     @Test
	     public void checkEnableDisableAccountForUserTest() throws Exception {
			  
			    Role role = new Role(1);
				User user5 = new User(1, role, 1, "D", "smita", "123456", "A", "sam", null, null);
				UserprofileId id = new UserprofileId(1, 1);
				Userprofile userprofile5 = new Userprofile(id, user5, "smita", "khot", "Pune", "Maharshtra", "India",
						"smita.khot25@gmail.com", "8237550368", "A");
	            customerRequest1 = new CustomerRequest(user5, userprofile5);
	            
				MockHttpServletRequest request = new MockHttpServletRequest();
				RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
				User user = customerRequest1.getUser();
				
				// MOCK CALLS
				when(userRepository.save(user)).thenReturn(user);
				when(userRepository.findById(user.getUserid())).thenReturn(Optional.of(user));
				
				//ACTUAL METHOD CALLS
				String result=adminServiceImpl.accountEnableDisable(user.getUserid(),"A");
				String result1=adminServiceImpl.accountEnableDisable(user.getUserid(),"A");
				String result2=adminServiceImpl.accountEnableDisable(user.getUserid(),"D");
				
				System.out.println(result);
				System.out.println(result1);
				System.out.println(result2);
				
				//TEST RESULTS
				assertThat(result).isEqualTo("Account status is enabled");
	            assertThat(result1).isEqualTo("Account status unchanged");
	            assertThat(result2).isEqualTo("Account status is disabled");
	           
	          }

	@AfterAll
	public static void clear() {
		customerRequest = null;
	
	}
}

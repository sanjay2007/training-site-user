
DROP TABLE IF EXISTS role ;

CREATE  TABLE IF NOT EXISTS role (
  `roleid` INT NOT NULL ,
  `rolename` VARCHAR(45) NULL ,
  PRIMARY KEY (`roleid`) );
-- -----------------------------------------------------
-- Table user
-- -----------------------------------------------------
DROP TABLE IF EXISTS user ;

CREATE  TABLE IF NOT EXISTS user (
  userid INT(11) NOT NULL ,
  profileid INT NULL DEFAULT NULL ,
  version VARCHAR(45) NULL DEFAULT NULL ,
  status VARCHAR(45) NULL DEFAULT NULL ,
  roleid INT NOT NULL ,
  password VARCHAR(45) NULL ,
  uniquecode VARCHAR(45) NULL ,
  resetpasswordstatus VARCHAR(45) NULL ,
  username VARCHAR(45) NOT NULL ,
  profileupdatestatus VARCHAR(45) NULL ,
  PRIMARY KEY (userid) ,
  UNIQUE INDEX uniquecode_UNIQUE (uniquecode ASC) ,
  CONSTRAINT roleid
    FOREIGN KEY (roleid)
    REFERENCES role (roleid)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);




-- -----------------------------------------------------
-- Table userprofile
-- -----------------------------------------------------
DROP TABLE IF EXISTS userprofile ;

CREATE  TABLE IF NOT EXISTS userprofile (
  profileid INT NOT NULL ,
  userid INT(11) NOT NULL ,
  version INT NULL ,
  firstname VARCHAR(45) NULL ,
  lastname VARCHAR(45) NULL ,
  city VARCHAR(45) NULL ,
  state VARCHAR(45) NULL ,
  country VARCHAR(45) NULL ,
  email VARCHAR(45) NULL ,
  mobile VARCHAR(45) NULL ,
  PRIMARY KEY (profileid, userid) ,
    CONSTRAINT userid
    FOREIGN KEY (userid)
    REFERENCES user (userid)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
;
